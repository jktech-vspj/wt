<?php
    header('Content-Type: application/json; charset=utf-8');

    $servername = "localhost";
    $username = "hrdlic30";
    $password = "Tis*3622359";
    $database = "hrdlic30";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 

    catch(PDOException $e) {
        $responseError = array(
            "success" => false,
            "code" => 500,
            "message" => "Nepodařilo se připojit k databázi"
        );

        http_response_code(500);
        echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
        exit();
    }
    
    $method = $_SERVER['REQUEST_METHOD'];

    if ($method == 'GET') {
        try {
            $result = $conn->query("SELECT firstname, lastname, userID as ID FROM users");
            $data = $result->fetchAll(PDO::FETCH_ASSOC);
        }

        catch(PDOException $e) {
            $responseError = array(
                "success" => false,
                "code" => 500,
                "message" => "Nepodařilo se získat data z databáze"
            );
    
            http_response_code(500);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        $response = array(
            "success" => true,
            "length" => sizeof($data),
            "data" => $data
        );

        echo(json_encode($response, JSON_UNESCAPED_UNICODE));
    }

    else if ($method == 'POST') {
        if(isset($_GET["firstname"]) == false || isset($_GET["lastname"]) == false) {
            $responseError = array(
                "success" => false,
                "code" => 400,
                "message" => "Vyplňte všechny parametry"
            );
    
            http_response_code(400);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        if(empty($_GET["firstname"]) || empty($_GET["lastname"])) {
            $responseError = array(
                "success" => false,
                "code" => 400,
                "message" => "Prázdné parametry nejsou povoleny"
            );
    
            http_response_code(400);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        try {
            $result = $conn->prepare("INSERT INTO users (firstname, lastname) VALUES (?,?)");
            $result->execute(array(htmlspecialchars($_GET["firstname"]), htmlspecialchars($_GET["lastname"])));
        }

        catch(PDOException $e) {
            $responseError = array(
                "success" => false,
                "code" => 500,
                "message" => "Nepodařilo se aktualizovat data"
            );
    
            http_response_code(500);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        $responseSuccess = array(
            "success" => true,
            "message" => "Uživatel byl úspěšně přidán"
        );

        echo(json_encode($responseSuccess, JSON_UNESCAPED_UNICODE));
        exit();
    }

    else if ($method == 'PUT') {
        if(isset($_GET["firstname"]) == false || isset($_GET["lastname"]) == false || isset($_GET["id"]) == false) {
            $responseError = array(
                "success" => false,
                "code" => 400,
                "message" => "Vyplňte všechny parametry"
            );
    
            http_response_code(400);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        if(is_numeric($_GET["id"]) == false) {
            $responseError = array(
                "success" => false,
                "code" => 400,
                "message" => "Neplatné ID uživatele"
            );
    
            http_response_code(400);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        if(empty($_GET["firstname"]) || empty($_GET["lastname"])) {
            $responseError = array(
                "success" => false,
                "code" => 400,
                "message" => "Prázdné parametry nejsou povoleny"
            );
    
            http_response_code(400);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        try {
            $result = $conn->prepare("UPDATE users SET firstname = ?, lastname = ? WHERE userID = ?");
            $result->execute(array(htmlspecialchars($_GET["firstname"]), htmlspecialchars($_GET["lastname"]), $_GET["id"]));
        }

        catch(PDOException $e) {
            $responseError = array(
                "success" => false,
                "code" => 500,
                "message" => "Nepodařilo se upravit data z databáze"
            );
    
            http_response_code(500);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        $responseSuccess = array(
            "success" => true,
            "message" => "Uživatel byl úspěšně upraven"
        );

        echo(json_encode($responseSuccess, JSON_UNESCAPED_UNICODE));
        exit();
    }

    else if ($method == 'DELETE') {
        if(isset($_GET["id"]) == false) {
            $responseError = array(
                "success" => false,
                "code" => 400,
                "message" => "Vyplňte všechny parametry"
            );
    
            http_response_code(400);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        if(is_numeric($_GET["id"]) == false) {
            $responseError = array(
                "success" => false,
                "code" => 400,
                "message" => "Neplatné ID uživatele"
            );
    
            http_response_code(400);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        try {
            $result = $conn->prepare("DELETE FROM users WHERE userID = ?");
            $result->execute(array($_GET["id"]));
        }

        catch(PDOException $e) {
            $responseError = array(
                "success" => false,
                "code" => 500,
                "message" => "Nepodařilo se odebrat data z databáze"
            );
    
            http_response_code(500);
            echo(json_encode($responseError, JSON_UNESCAPED_UNICODE));
            exit();
        }

        $responseSuccess = array(
            "success" => true,
            "message" => "Uživatel byl úspěšně odebrán"
        );

        echo(json_encode($responseSuccess, JSON_UNESCAPED_UNICODE));
        exit();
    }


?>
