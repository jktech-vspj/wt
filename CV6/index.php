<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ukol 6</title>
    <script src="js.js" defer></script>
</head>
<body>
    <h1>Data stažená z restAPI: </h1>
    <table id="response-table">
        <thead>
            <tr>
                <td>ID</td>
                <td>Jméno</td>
                <td>Příjmení</td>
            </tr>
        </thead>
        <tbody></tbody>
    </table>

    <i>URL pro restAPI: ./rest.php</i>

    <h1>Screenshoty: </h1>
    <img src="./Images/ukol_1.png" />
    <img src="./Images/ukol_2.png" />
    <img src="./Images/ukol_4.png" />
    <img src="./Images/ukol_5.png" />
</body>
</html>
