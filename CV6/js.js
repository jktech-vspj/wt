'use strict';

async function fetchData() {
    let text;

    try {
        const response = await fetch("rest.php");

        if(response.ok == false) {
            throw new Error("Nepodařilo se stáhnout data");
        }

        text = await response.json();
    }

    catch(e) {

    }

    const tbody = document.querySelector("tbody");
    
    for(const user of text.data) {
        const row = document.createElement('tr');

        console.log(user);

        const idColumn = document.createElement("td");
        idColumn.textContent = user.ID;
        row.appendChild(idColumn);

        const firstnameColumn = document.createElement("td");
        firstnameColumn.textContent = user.firstname;
        row.appendChild(firstnameColumn);

        const lastnameColumn = document.createElement("td");
        lastnameColumn.textContent = user.firstname;
        row.appendChild(lastnameColumn);

        tbody.appendChild(row);
    }
}

fetchData();