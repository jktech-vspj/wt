<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ukol 5</title>
	<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</head>
<body>
	<form action="" method="post" enctype="multipart/form-data">
	  	<input type="file" name="fileToUpload" id="fileToUpload" accept=".doc,.docx">
	  	<input type="submit" value="Nahrát dokument" name="submit">
	</form>

	<?php
		if($_FILES && isset($_FILES["fileToUpload"]["name"])) {
			$target_dir = "./";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
	
			if(isset($_POST["submit"])) {
				if($imageFileType != "doc" && $imageFileType != "docx") {
					  echo "Soubor je ve špatném formátu";
					  $uploadOk = 0;
				}
	
				if($uploadOk == 1) {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
						echo "Soubor " . htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])) . " nahrán úspěšně.";
					} 
					
					else {
						echo "Soubor nenahrán";
					}
				}
			}
		}
	?>
</body>
</html>
