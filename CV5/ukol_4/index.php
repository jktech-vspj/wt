<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ukol 3</title>
	<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
	<script src="js.js" defer></script>
</head>
<body>
	<label label for="options">Vyberte vyrobce routeru:</label>

	<select name="options" id="options">
		<option value="Mikrotik">Mikrotik</option>
		<option value="D-Link">D-Link</option>
		<option value="Cisco">Cisco</option>
		<option value="HPE">HPE</option>
	</select> 
	<div id="selectionValue"></div>

</body>
</html>