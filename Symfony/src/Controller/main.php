<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class main extends AbstractController
{
    #[Route('/', name: 'root')]
    public function index(): Response
    {
        return new Response('
            <h2>Symfony WT</h2>
            <ul>
                <li><a href="/cv7/">Cviceni 7</a></li>
                <li><a href="/cv8/">Cviceni 8</a></li>
                <li><a href="/cv9/">Cviceni 9</a></li>
                <li><a href="/student/">Cviceni 10</a></li>
                <li><a href="/cv11/">Cviceni 10</a></li>
            </ul>
        ');
    }
}