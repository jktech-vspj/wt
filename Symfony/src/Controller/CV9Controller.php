<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CV9Controller extends AbstractController
{
    #[Route('/cv9/', name: 'cv9_home')]
    public function index(): Response
    {
        return new Response('
            <h2>Cvičení 9</h2>
            <ul>
                <li><a href="/cv9/poslipole">Ukol 1</a></li>
                <li><a href="/cv9/poslipolevpoli">Ukol 2</a></li>
                <li><a href="/cv9/css">Ukol 3</a></li>
                <li><a href="/cv9/ukol_4">Ukol 4</a></li>
                <li><a href="/cv9/bootstrap">Ukol 5</a></li>
            </ul>
        ');
    }

    #[Route('/cv9/poslipole/', name: 'cv9_ukol_1')]
    public function poslipole(): Response
    {
        $pole = array(1,2,3,4,5,6,7,8,9);
        return $this->render("CV9/poslipole.html.twig",[
            "pole" => $pole
        ]);
    }

    #[Route('/cv9/poslipolevpoli/', name: 'cv9_ukol_2')]
    public function poslipolevpoli(): Response
    {
        $lide = [
            ['jmeno' => 'Jan', 'prijmeni' => 'Novák'],
            ['jmeno' => 'Petr', 'prijmeni' => 'Doležal'],
            ['jmeno' => 'Marie', 'prijmeni' => 'Novotná'],
        ];

        return $this->render("CV9/poslipolevpoli.html.twig");
    }

    #[Route('/cv9/css/', name: 'cv9_ukol_3')]
    public function css(): Response
    {
        return $this->render("CV9/css.html.twig");
    }

    #[Route('/cv9/ukol_4/', name: 'cv9_ukol_4')]
    public function ukol_4(): Response
    {
        $pole = array(1,2,3,4,5,6,7,8,9);
        return $this->render("CV9/poslipolecolor.html.twig",[
            "pole" => $pole
        ]);
    }

    #[Route('/cv9/bootstrap/', name: 'cv9_ukol_5')]
    public function bootstrap(): Response
    {
        $pole = array(1,2,3,4,5,6,7,8,9);
        return $this->render("CV9/bootstrap.html.twig");
    }
}