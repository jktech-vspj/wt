<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CV8Controller extends AbstractController
{
    #[Route('/cv8/', name: 'cv8_home')]
    public function index(): Response
    {
        return new Response('
            <h2>Cvičení 8</h2>
            <ul>
                <li><a href="/cv8/ukol_1?jmeno=franta">Ukol 1</a></li>
                <li><a href="/cv8/ukol_2">Ukol 2</a></li>
            </ul>
        ');
    }

    #[Route('/cv8/ukol_1/', name: 'cv8_ukol1')]
    public function ukol1(Request $request): Response
    {
        $jmeno = $request->get('jmeno');

        $array = array(
            "jmeno" => "Jakub",
            "prijmeni" => "Hrdlicka"
        );

        dump($array);

        return $this->render("CV8/index.html.twig", [
            "jmeno" => $jmeno
        ]);
    }

    #[Route('/cv8/ukol_2/', name: 'cv8_ukol2_home')]
    public function cviceniHome(): Response
    {
        return $this->render("CV8/cviceni.html.twig", [
            "cv" => "Nevybrano"
        ]);
    }

    #[Route('/cv8/ukol_2/{cv}', name: 'cv8_ukol2')]
    public function cviceni(string $cv): Response
    {
        return $this->render("CV8/cviceni.html.twig", [
            "cv" => $cv
        ]);
    }
}