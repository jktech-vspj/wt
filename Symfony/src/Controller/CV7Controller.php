<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class CV7Controller extends AbstractController
{
    #[Route('/cv7/', name: 'cv7_home')]
    public function index(): Response
    {
        return new Response('
            <h2>Cvičení 7</h2>
            <ul>
                <li><a href="/cv7/text">Ukol 1</a></li>
                <li><a href="/cv7/twig">Ukol 2</a></li>
            </ul>
        ');
    }

    #[Route('/cv7/text', name: 'cv7_text')]
    public function text(): Response
    {
        return new Response('
            Ahoj svete, <b style="color: blue;">Hrdlicka</b>
        ');
    }

    #[Route('/cv7/twig/', name: 'cv7_twig')]
    public function twig(): Response
    {
        return $this->render("CV7/base.html.twig", [
            "firstname" => "Jakub",
            "lastname" => "Hrdlicka"
        ]);
    }
}