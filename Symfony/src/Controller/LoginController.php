<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cv11')]
class LoginController extends AbstractController
{
    #[Route('/login', name: 'cv11_login')]
    public function index(): Response
    {
        return $this->render('cv11/index.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }
}
