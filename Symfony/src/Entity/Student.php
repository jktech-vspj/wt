<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StudentRepository::class)]
class Student
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $studentID = null;

    #[ORM\Column(length: 255)]
    private ?string $jmeno = null;

    #[ORM\Column(length: 255)]
    private ?string $prijmeni = null;

    #[ORM\Column]
    private ?int $rocnik = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudentID(): ?int
    {
        return $this->studentID;
    }

    public function setStudentID(int $studentID): static
    {
        $this->studentID = $studentID;

        return $this;
    }

    public function getJmeno(): ?string
    {
        return $this->jmeno;
    }

    public function setJmeno(string $jmeno): static
    {
        $this->jmeno = $jmeno;

        return $this;
    }

    public function getPrijmeni(): ?string
    {
        return $this->prijmeni;
    }

    public function setPrijmeni(string $prijmeni): static
    {
        $this->prijmeni = $prijmeni;

        return $this;
    }

    public function getRocnik(): ?int
    {
        return $this->rocnik;
    }

    public function setRocnik(int $rocnik): static
    {
        $this->rocnik = $rocnik;

        return $this;
    }
}
