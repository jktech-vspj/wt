$(document).ready(function() {
    $("#select-all").change(function() {
        if($(this).is(":checked")) {
            $(":checkbox").prop('checked', true);
        }

        else {
            $(":checkbox").prop('checked', false);
        }
    });

    $("#select-health").change(function() {
        if($(this).is(":checked")) {
            $(".health").prop('checked', true);
        }

        else {
            $(".health").prop('checked', false);
        }
    });

    $("#select-social").change(function() {
        if($(this).is(":checked")) {
            $(".social").prop('checked', true);
        }

        else {
            $(".social").prop('checked', false);
        }
    });
});


