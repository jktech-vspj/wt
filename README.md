# Webové technologie

### Rozdělení cvičení
- 1-2 - HTML + jQuery
- 3-6 - PHP
- 7-10 - Symfony

### Popis
Vypracovaná cvičení do předmětu Webové Technologie na VŠPJ. Cvičení jsou vypracovaná pro **Symfony 6.3.7**. Všechna CV ze Symfony jsou v jednom projektu ve složce **Symfony/**. Semestrální projekt je v **[jiném repozitáři](https://gitlab.com/jktech-vspj/wt-projekt)**.

### Použití

```git
git clone git@gitlab.com:jktech-vspj/wt.git
```

#### Instalace závislostí pro symfony

Přejděte do složky se symfony projektem
```
cd wt/Symfony
```

Nainstalujte závislosti
```bash
composer install
```

Spuštění symfony serveru
```bash
symfony server:start
```

### Pokud vám nefunguje příkaz `composer` nebo `symfony`, ujistěte se že máte symfony a composer správně nainstalované:

```bash
symfony check:requirements
```