$( function() {
    const availableTags = [
        "Ubiquiti",
        "MikroTik",
        "TP-Link",
        "XtendLan",
        "Turris",
        "Aruba",
        "Cisco",
        "Zyxel",
        "D-Link",
        "Huawei",
        "Dell",
        "Asus",
        "Tenda",
        "NETGEAR",
        "HPE",
        "Xiaomi",
        "Google WiFi",
        "Linksys",
        "Synology",
        "Mercusys"
    ];

    $( "#tags" ).autocomplete({
        source: availableTags
    });
} );
